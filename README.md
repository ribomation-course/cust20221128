# Modern C++, 3 days
## 2022 November 28&ndash;30

Welcome to this course. Here you will find
* Installation instructions
* Preparation task
* Solutions to the programming exercises
* Sources to the demo programs

_N.B._ Solutions and Demos programs will be pushed during the course.

## Links
* [Course Details, _primary_](https://www.ribomation.se/programmerings-kurser/cxx/cxx-17/)
* [Course Details, _secondary_](https://www.ribomation.se/programmerings-kurser/cxx/cxx-memory-constrained-systems/)
* [Installation Instructions](./installation-instructions.md)
* [Preparation Tasks](./preparation-tasks.md) - _Do these before the course starts_


# Course GIT Repo
It's recommended that you keep the git repo and your solutions separated.
Create a dedicated directory for this course and a subdirectory for
each chapter. Get the course repo initially by a `git clone` operation

![Git Clone](img/git-clone.png)

    mkdir -p ~/cxx-course/my-solutions
    cd ~/cxx-course
    git clone <git HTTPS clone link> gitlab

During the course, solutions will be push:ed to this repo and you can get these by
a `git pull` operation

    cd ~/cxx-course/gitlab
    git pull


# Build Solution/Demo Programs
The solutions and demo programs are all using CMake as the build tool. CMake is a cross-platform generator
tool that can generate makefiles and other build tool files. It is also the project descriptor for JetBrains
CLion, which is my IDE of choice for C/C++ development.

You don't have to use CLion in order to compile and run the sources.
What you do need to have; are `cmake`, `make` and `gcc/g++` all installed.
When you want to build a solution or demo:

First change into its project directory `cd path/to/some/solutions/dir`, then run the commands below
and the executable will be in the `./bld/` directory.

    cd some/project/folder/
    cmake -S . -B bld
    cmake --build bld
    ./bld/the-app-exe


# Interesting Videos & Articles

## Presentations
* [CppCon 2018: Bjarne Stroustrup “Concepts: The Future of Generic Programming (the future is here)”](https://youtu.be/HddFGPTAmtU)
* [CppCon 2015: Eric Niebler "Ranges for the Standard Library"](https://youtu.be/mFUXNMfaciE)
* [CppCon 2015: Bjarne Stroustrup “Writing Good C++14”](https://youtu.be/1OEu9C51K2A)
* [CppCon 2018: Jonathan Boccara “105 STL Algorithms in Less Than an Hour”](https://youtu.be/2olsGf6JIkU)
* [CppCon 2021: Thamara Andrade "Online Tools Every C++ Developers Should Know"](https://youtu.be/UztsWf7F_Sc)
* [CppCon 2016: Jason Turner “Rich Code for Tiny Computers: A Simple Commodore 64 Game in C++17”](https://youtu.be/zBkNBP00wJE)
* [CppCon 2018: Michael Caisse “Modern C++ in Embedded Systems - The Saga Continues”](https://youtu.be/LfRLQ7IChtg)


## Articles
* [AAA Style (Almost Always Auto)](https://herbsutter.com/2013/08/12/gotw-94-solution-aaa-style-almost-always-auto/)
* [7 Handy Functions for Associative Containers in Modern C++](https://www.cppstories.com/2021/handy-map-functions/)
* [C++ Community Resources](https://hackingcpp.com/cpp/community.html)
* [The World Map of C++ STL Algorithms](https://www.fluentcpp.com/getthemap/)
* [Objects? No, thanks! (Using C++ effectively on small systems)](https://www.embedded.com/objects-no-thanks-using-c-effectively-on-small-systems/)


## Books
* [Modern C++ Programming Cookbook - 2nd ed](https://www.oreilly.com/library/view/modern-c-programming/9781800208988/)
* [Embedded Programming with Modern C++ Cookbook](https://www.packtpub.com/product/embedded-programming-with-modern-c-cookbook/9781838821043)
* [C++17 - The Complete Guide](https://www.cppstd17.com/)
* [Beautiful C++: 30 Core Guidelines for Writing Clean, Safe, and Fast Code](http://www.gregcons.com/KateBlog/BeautifulC30CoreGuidelinesForWritingCleanSafeAndFastCode.aspx)
* [Top 10 Books For Learning C++17](https://www.mycplus.com/computer-books/programming-books/books-for-learning-cpp17/)
* [Awesome Modern C++](https://awesomecpp.com/)
* [Real-Time-C++ Book, 4th ed](https://link.springer.com/book/10.1007/978-3-662-62996-3)


## Repositories
* [Real-Time-C++ Book GitHub repo](https://github.com/ckormanyos/real-time-cpp)


## References
* [CppReference](https://en.cppreference.com/w/)
* [C++ Core Guidelines](http://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines)


## Online Compilers and Tools
* [Compiler Explorer](https://godbolt.org/)
* [C++ Insights](https://cppinsights.io/)
* [C++ Quick Benchmarks](http://quick-bench.com/)
* [Coliru - Online Compiler](https://coliru.stacked-crooked.com/)
* [WandBox - Online Compiler](https://wandbox.org/)


***
*If you have any questions, don't hesitate to contact me*<br>
**Jens Riboe**<br/>
Ribomation AB<br/>
[jens.riboe@ribomation.se](mailto:jens.riboe@ribomation.se)<br/>
[www.ribomation.se](https://www.ribomation.se)<br/>

