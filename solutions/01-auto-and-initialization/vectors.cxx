#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <string>

using std::cout;
using std::string;
using namespace std::string_literals;

template<typename T>
using ValueType = std::vector<T>;

int main() {
    auto vec = std::vector<string>{
            "one"s, "two"s, "three"s
    };
    for (auto const &s: vec) cout << s << " ";
    cout << "\n";
    auto set = std::set<string>{
            "one"s, "zzz"s, "two"s, "three"s, "aaa"s
    };
    for (auto const &s: set) cout << s << " ";

    auto map = std::map<string, std::vector<int>>{};
}
