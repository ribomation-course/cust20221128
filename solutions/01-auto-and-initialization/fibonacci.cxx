#include <iostream>
#include <map>

using std::cout;

auto fibonacci(unsigned n) {
    if (n == 0) return 0U;
    if (n == 1) return 1U;
    return fibonacci(n - 2) + fibonacci(n - 1);
}

struct Result {
    unsigned arg;
    unsigned res;

    Result(unsigned int arg, unsigned int res) : arg(arg), res(res) {}
};

auto compute(unsigned n) -> Result {
    return {n, fibonacci(n)};
}

auto table(unsigned n) /*-> std::map<unsigned, unsigned>*/ {
    auto tbl = std::map<unsigned, unsigned>{};
    for (auto k = 1U; k <= n; ++k) {
        auto [arg, res] = compute(k);
        tbl[arg] = res;
    }
    return tbl;
}


int main() {
    auto const N = 10;
    {
        auto result = fibonacci(N);
        cout << "fib(" << N << ") = " << result << "\n";
    }
    {
        auto [a, r] = compute(N);
        cout << "fib(" << a << ") = " << r << "\n";
    }
    cout << "---\n";
    {
        for (auto [a, r]: table(N))
            cout << "fib(" << a << ") = " << r << "\n";
    }
}
