#include <iostream>
#include "simple-data.hxx"
using std::cout;

int main() {
    {
        char buf[sizeof(SimpleData)];
        auto ptr = new (buf) SimpleData{10};
        cout << "*ptr: " << *ptr << " @ " << ptr << "\n";

        auto p = reinterpret_cast<int*>(ptr);
        cout << "p[0]: " << p[0] << ", " << *(p + 0) << "\n";
        cout << "p[1]: " << p[1] << ", " << *(p + 1) << "\n";
        cout << "p[2]: " << p[2] << ", " << *(p + 2) << "\n";
        ptr->~SimpleData();
        // delete ptr;  //this will not work!
    }

    {
        auto mem = new unsigned char[sizeof(SimpleData)];
        auto ptr = new (mem) SimpleData{10};
        cout << "*ptr: " << *ptr << " @ " << ptr << "\n";
        ptr->~SimpleData();
        delete[] mem;
    }
}
