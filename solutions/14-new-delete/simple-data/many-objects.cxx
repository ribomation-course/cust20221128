#include <iostream>
#include <stdexcept>
#include <string>
#include <memory>
#include <cstring>
#include <cerrno>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "simple-data.hxx"

using std::runtime_error;
using std::cout;
using namespace std::literals::string_literals;

struct Disposer {
    void *segment;
    size_t size;
    int N;

    Disposer(void *segment, size_t size, int N) : segment(segment), size(size), N(N) {}

    void operator()(void *) const {
        cout << "-- disposing " << N << " objects\n";
        for (auto k = 0; k < N; ++k) {
            auto *ptr = reinterpret_cast<SimpleData *>(segment + k * sizeof(SimpleData));
            ptr->~SimpleData();
        }
        auto rc = ::munmap(segment, size);
        if (rc == -1) {
            throw runtime_error{"munmap failed: "s + strerror(errno)};
        }
    }
};


int main(int argc, char **argv) {
    auto const N = argc == 1 ? 10 : std::stoi(argv[1]);
    size_t size = N * sizeof(SimpleData);
    auto memblk = ::mmap(nullptr, size, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    if (memblk == MAP_FAILED) {
        throw runtime_error{"mmap failed: "s + strerror(errno)};
    }
    auto segment = std::unique_ptr<void, Disposer>(memblk, Disposer{memblk, size, N});

    for (auto k = 0; k < N; ++k) {
        auto addr = segment.get() + k * sizeof(SimpleData);
        auto ptr = new(addr) SimpleData{k + 1};
        cout << "segment[" << k << "] @ " << ptr << "\n";
    }

    cout << "----\n";
    for (auto k = 0; k < N; ++k) {
        SimpleData *ptr = &reinterpret_cast<SimpleData *>(segment.get())[k];
        cout << "segment[" << k << "] = " << *ptr << "\n";
    }
}
