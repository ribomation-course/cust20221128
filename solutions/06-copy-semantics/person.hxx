#pragma once

#include <iostream>
#include <sstream>
#include <cstring>

namespace ribomation::domain {
    using std::cout;

    inline auto strClone(char const* str) -> char* {  //strdup() is using malloc()
        if (str == nullptr) return nullptr;
        return ::strcpy(new char[::strlen(str) + 1], str);
    }

    class Person {
        char* name = nullptr;
        unsigned age = 0;

    public:
        ~Person() {
            cout << "~Person @ " << this << "\n";
            delete[] name;
        }

        Person() {
            cout << "Person{} @ " << this << "\n";
            name = strClone("");
        }

        Person(const char* n, unsigned a) : name{strClone(n)}, age{a} {
            cout << "Person{const char* " << n << ", " << a << "} @ " << this << "\n";
        }

        Person(Person const& that) : name{strClone(that.name)}, age{that.age} {
            cout << "Person{const Person& " << &that << "} @ " << this << "\n";
        }

        Person& operator=(Person const& rhs) noexcept {
            if (this != &rhs) {
                delete[] name;
                name = strClone(rhs.name);
                age  = rhs.age;
            }
            cout << "operator={Person const& " << &rhs << "} @ " << this << "\n";
            return *this;
        }

        auto incrAge() {
            return ++age;
        }

        [[nodiscard]] auto toString() const -> std::string {
            auto buf = std::ostringstream{};
            buf << "Person(" << (name ? name : "??") << ", " << age << ") @ " << this;
            return buf.str();
        }
    };

}

