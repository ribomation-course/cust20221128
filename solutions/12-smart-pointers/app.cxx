#include <iostream>
#include "usecase-unique-ptr.hxx"
#include "usecase-shared-ptr.hxx"

int main() {
    use_unique_ptr();
    std::cout << "------\n";
    use_shared_ptr();
}
