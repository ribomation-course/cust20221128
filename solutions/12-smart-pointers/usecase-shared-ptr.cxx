#include <iostream>
#include <memory>
#include "person.hxx"

using namespace std::string_literals;
using std::cout;
using std::endl;
using std::shared_ptr;
using std::make_shared;
using ribomation::Person;

auto func2(shared_ptr<Person> q) -> shared_ptr<Person> {
    q->incrAge();
    cout << "[func2] q: " << *q << " [" << q.use_count() << "]" << endl;
    return q;
}

auto func1(shared_ptr<Person> q) -> shared_ptr<Person> {
    q->incrAge();
    cout << "[func1] q: " << *q << " [" << q.use_count() << "]" << endl;
    return func2(q);
}

void use_shared_ptr() {
    auto justin = make_shared<Person>("Justin Time"s, 37);
    cout << "[use_shared_ptr] justin: " << *justin << " [" << justin.use_count() << "]" << endl;
    {
        auto p = func1(justin);
        cout << "[use_shared_ptr] p: " << *p << " [" << p.use_count() << "]" << endl;
    }
    cout << "[use_shared_ptr] justin: " << *justin << " [" << justin.use_count() << "]" << endl;
}
