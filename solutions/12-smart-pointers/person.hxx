#pragma once

#include <iostream>
#include <string>
#include <utility>

namespace ribomation {
    using std::string;
    using std::move;
    using std::cout;
    using std::endl;
    using std::ostream;

    class Person {
        const string name;
        unsigned     age;
        
    public:
        Person(string name_, unsigned int age_) : name{std::move(name_)}, age{age_} {
            cout << "+Person{" << name << ", " << age << "} @ " << this << endl;
        }

        ~Person() {
            cout << "~Person() @ " << this << endl;
        }

        Person(const Person&) = delete;
        auto operator=(const Person&) -> Person& = delete;

        unsigned incrAge() {
            ++age;
            return age;
        }

        friend auto operator <<(ostream& os, const Person& p) -> ostream& {
            return os << "Person{" << p.name << ", " << p.age << "} @ " << &p;
        }
    };

}

