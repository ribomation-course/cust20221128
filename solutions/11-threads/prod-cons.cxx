#include <iostream>
#include <iomanip>
#include <locale>
#include <string>
#include <thread>
#include "message-queue.hxx"

using namespace std;
using namespace std::literals;
using namespace ribomation::threads;

struct Producer {
    const unsigned max;
    MessageQueue<unsigned>& out;

    Producer(unsigned max, MessageQueue<unsigned>& out) : max(max), out(out) {}

    void operator()() {
        for (auto k = 1U; k <= max; ++k) out.put(k);
        out.put(0);
    }
};

struct Consumer {
    MessageQueue<unsigned>& in;

    explicit Consumer(MessageQueue<unsigned>& in) : in(in) {}

    void operator()() {
        for (auto msg = in.get(); msg != 0; msg = in.get())
            cout << "C: " << setw(6) << msg << endl;
    }
};

int main() {
    cout.imbue(locale{"en_US.Utf8"});

    auto N    = 10'000U;
    auto Q    = MessageQueue<unsigned>{16};
    auto prod = thread{Producer{N, Q}};
    auto cons = thread{Consumer{Q}};
    prod.join();
    cons.join();
}
