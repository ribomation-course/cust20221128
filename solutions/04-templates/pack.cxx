#include <iostream>
#include <iomanip>
#include <limits>
#include <type_traits>
#include <cstdint>

using std::cout;
using std::numeric_limits;
using std::is_unsigned_v;
using std::hex;
using std::uint8_t;
using std::uint16_t;

template<typename SrcType, typename DstType>
DstType pack(SrcType high, SrcType low) {
    static_assert(is_unsigned_v<SrcType>);
    static_assert(is_unsigned_v<DstType>);

    constexpr auto srcSize = numeric_limits<SrcType>::digits;
    constexpr auto dstSize = numeric_limits<DstType>::digits;
    static_assert((2 * srcSize) == dstSize);

    return static_cast<DstType>(high << srcSize) | low;
}

int main() {
    cout << hex << pack<uint8_t, uint16_t>(0xAB, 0xDC) << "\n";
    cout << hex << pack<uint16_t, uint32_t>(0x1234, 0x5678) << "\n";
}
