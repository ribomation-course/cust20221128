#include <iostream>

using std::cout;
using std::endl;
using namespace std::string_literals;

template<typename T>
T maximum(T a, T b) { return a >= b ? a : b; }

template<typename T>
T maximum(T a, T b, T c) { return maximum(maximum(a, b), c); }

int main() {
    cout << "int   : " << maximum(5, 10, 30) << endl;
    cout << "double: " << maximum(5.1, 10.3, 30.9) << endl;
    cout << "string: " << maximum("AA"s, "aaa"s, "aaaaa"s) << endl;
}
