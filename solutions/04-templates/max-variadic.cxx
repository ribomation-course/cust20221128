#include <iostream>
#include <string>
#include <random>
#include <cstdio>

using std::cout;
using std::endl;
using namespace std::string_literals;

template<typename T>
T maximum(T a, T b) { return a >= b ? a : b; }

template<typename T, typename... Args>
T maximum(T a, T b, Args... args) {
    return maximum(maximum(a, b), args...);
}

template<typename... Args>
auto fn(std::string const &name, Args... args) -> std::string {
    char buf[1024];
    auto N = sizeof...(args);
    auto fmt = name + "(%d"s;
    while (--N > 0) fmt += ",%d";
    fmt += "): "s;
    ::sprintf(buf, fmt.c_str(), args...);
    return buf;
}

int main() {
    cout << "int   : " << maximum(5, 10, 30) << endl;
    cout << "double: " << maximum(5.1, 10.3, 30.9) << endl;
    cout << "string: " << maximum("AA"s, "aaa"s, "aaaaa"s) << endl;

    {
        auto r = std::random_device{};
        auto rect = std::uniform_int_distribution<unsigned>{1, 100};
        auto a = rect(r);
        auto b = rect(r);
        auto c = rect(r);
        auto d = rect(r);
        auto e = rect(r);
        auto f = rect(r);
        auto g = rect(r);
        auto h = rect(r);
        cout << fn("max"s, a, b, c, d, e, f, g, h)
        << maximum(a, b, c, d, e, f, g, h) << "\n";
    }
}
