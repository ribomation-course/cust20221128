#include <iostream>
using std::cout;

void fn(int a, int& b, int&& c, int&& d) {
    a *= 10;
    b *= 20;
    c *= 30;
    d *= 40;
}

int main() {
    auto number = 10;
    cout << "[before] number=" << number << "\n";
    fn(number, number, number * 3, std::move(number));
    cout << "[after]  number=" << number << "\n";
}
