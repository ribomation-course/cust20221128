#include <iostream>
#include <algorithm>

using std::cout;

int main() {
    int numbers[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    auto const N = sizeof(numbers) / sizeof(numbers[0]);

    auto print = [](auto n) { cout << n << " "; };
    std::for_each(numbers, numbers + N, print);cout<<"\n";

    auto factor = 42;
    auto mult = [factor](auto n) {return n * factor;};
    std::transform(numbers, numbers + N, numbers, mult);

    std::for_each(numbers, numbers + N, print);
}
