#include <iostream>
#include <functional>

using std::cout;

int count_if(int* first, int* last, std::function<bool (int)> isTrue) {
    int cnt = 0;
    for (; first != last; ++first) if (isTrue(*first)) ++cnt;
    return cnt;
}

int main() {
    int numbers[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20};
    auto const N = sizeof(numbers) / sizeof(numbers[0]);

    cout << "# even: " << count_if(numbers, numbers + N,
                                   [](int n) { return n % 2 == 0; }) << "\n";
    cout << "# odd : " << count_if(numbers, numbers + N,
                                  [](int n) { return n % 2 != 0; }) << "\n";
}
