#include <iostream>
#include <fstream>
#include <string>
#include <stdexcept>
#include <set>
#include <unordered_set>
#include <algorithm>
#include <cctype>

using namespace std::literals::string_literals;
using std::cout;
using std::endl;
using std::istream;
using std::ifstream;
using std::string;
using std::unordered_multiset;
using std::multiset;
using std::invalid_argument;

auto strip(string word) -> string;
auto lc(string word) -> string;
auto load(istream& in, unsigned minSize = 4U) -> unordered_multiset<string>;
void print(multiset<string> const& freqs);

int main(int argc, char** argv) {
    auto filename = (argc == 1) ? "../musketeers.txt"s : argv[1];
    auto file     = ifstream{filename};
    if (!file) throw invalid_argument{"cannot open "s + filename};

    auto words = load(file);
    auto freqs = multiset<string>{words.begin(), words.end()};
    print(freqs);
}

auto load(istream& in, unsigned minSize) -> unordered_multiset<string> {
    auto      words = unordered_multiset<string>{};
    for (auto word  = ""s; in >> word;) {
        word = strip(word);
        if (word.size() >= minSize) words.insert(lc(word));
    }
    return words;
}

void print(multiset<string> const& freqs) {
    for (auto it = freqs.begin(); it != freqs.end();) {
        auto word = *it;
        auto freq = freqs.count(word);
        cout << word << ": " << freq << endl;
        advance(it, freq);
    }
}

auto strip(string word) -> string {
    auto result = ""s;
    copy_if(word.begin(), word.end(), back_inserter(result), [](auto ch) {
        return isalpha(ch);
    });
    return result;
}

auto lc(string word) -> string {
    transform(word.begin(), word.end(), word.begin(), [](auto ch) {
        return tolower(ch);
    });
    return word;
}
