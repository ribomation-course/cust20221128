#include <iostream>
#include <string>

using std::cout;
using namespace std::string_literals;

struct NoCopy {
    NoCopy() = default;
    NoCopy(NoCopy const &) = delete;
    auto operator =(NoCopy const &) -> NoCopy& = delete;
};

// class Person : public NoCopy {
struct Person : NoCopy {
    std::string name;
    unsigned age;
    Person() = default;
    Person(const std::string &name, unsigned int age)
    : name(name), age(age) {}
};

void func(NoCopy x) {
    //cout << "[func] " << x.name << ", " << x.number << "\n";
}

int main() {
    auto obj = Person{"Nisse"s, 42};
    cout << "[main] " << obj.name << ", " << obj.age << "\n";
   // func(obj);

    auto obj2 = Person{};
   // obj2 = obj;
    cout << "[main] (2) " << obj2.name << ", " << obj2.age << "\n";

    Person p1{}, p2{p1};
   // p2 = p1;
}
