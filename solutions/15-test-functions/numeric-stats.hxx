#include <tuple>
#include <vector>
#include <numeric>
#include <algorithm>
#include <stdexcept>


namespace ribomation {
    using namespace std::string_literals;
    using std::tuple;
    using std::make_tuple;
    using std::vector;
    using std::invalid_argument;
    using std::accumulate;
    using std::min_element;
    using std::max_element;

    template<typename T>
    auto numeric_stats(const vector<T>& data) -> tuple<double, T, T> {
        if (data.empty()) {
            throw invalid_argument("empty data vector<T>"s);
        }

        double sum = accumulate(data.begin(), data.end(), 0);
        T      min = *min_element(data.begin(), data.end());
        T      max = *max_element(data.begin(), data.end());

        return make_tuple(sum / data.size(), min, max);
    }

}

