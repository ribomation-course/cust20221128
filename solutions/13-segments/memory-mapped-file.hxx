#pragma once

#include <string>
#include <stdexcept>
#include <tuple>
#include <filesystem>

#include <cerrno>
#include <cstring>

#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>

namespace ribomation::memory {
    namespace fs = std::filesystem;
    using namespace std::literals::string_literals;
    using std::string;
    using std::tuple;

    inline auto map(const string& filename) -> tuple<void*, size_t> {
        int fd = ::open(filename.c_str(), O_RDWR);
        if (fd < 0) {
            throw std::invalid_argument{"cannot open '"s + filename + "': " + strerror(errno)};
        }

        auto filesize = fs::file_size(filename);
        auto segment = (char*) ::mmap(nullptr, filesize, PROT_READ | PROT_WRITE, MAP_PRIVATE, fd, 0);
        if (segment == MAP_FAILED) {
            throw std::runtime_error("mmap failed: "s + strerror(errno));
        }
        ::close(fd);

        return {segment, filesize};
    }

    class MemoryMappedFile {
        void* payload;
        size_t payloadSize;

    public:
        explicit MemoryMappedFile(string const& filename) {
            if (!fs::exists(filename)) throw std::invalid_argument{filename + " not found"s};
            auto t = map(filename);
            payload     = std::get<void*>(t);
            payloadSize = std::get<size_t>(t);
        }

        virtual ~MemoryMappedFile() {
            munmap(payload, payloadSize);
        }

        [[nodiscard]] char* data() const {
            return reinterpret_cast<char*>(payload);
        }

        [[nodiscard]] size_t size() const {
            return payloadSize;
        }

        MemoryMappedFile() = delete;
        MemoryMappedFile(const MemoryMappedFile&) = delete;
        MemoryMappedFile& operator=(const MemoryMappedFile&) = delete;
    };

}
