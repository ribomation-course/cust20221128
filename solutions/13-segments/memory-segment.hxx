#pragma once

#include <stdexcept>
#include <cstring>
#include <cerrno>
#include <sys/mman.h>

namespace ribomation::memory {
    using namespace std::string_literals;

    class MemorySegment {
        void* start;
        size_t size;
        
    public:
        explicit MemorySegment(size_t size_) : size{size_} {
            start = ::mmap(nullptr, size, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
            if (start == MAP_FAILED) {
                throw std::runtime_error{"mmap(2) failed: "s + ::strerror(errno)};
            }
        }
        ~MemorySegment() { munmap(start, size); }
        
        [[nodiscard]] void* begin() const { return start; }
        [[nodiscard]] void* end() const { return start + size; }
    };

}
