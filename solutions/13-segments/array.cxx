#include <iostream>
#include <string>
#include "memory-segment.hxx"
using std::cout;
using ribomation::memory::MemorySegment;

auto gauss_sum(long N) {
    return N * (N + 1) / 2;
}

int main(int argc, char** argv) {
    auto const N      = argc == 1 ? 1'000'000L : std::stol(argv[1]);
    auto       memory = MemorySegment{N * sizeof(int)};

    int* array = static_cast<int*>(memory.begin());
    for (auto k = 0; k < N; ++k) array[k] = k + 1;

    auto      sum = 0L;
    for (auto k   = 0; k < N; ++k) sum += array[k];

    cout.imbue(std::locale("en_US.utf8"));
    cout << "sum: " << sum << " (" << gauss_sum(N) << ")\n";
}
