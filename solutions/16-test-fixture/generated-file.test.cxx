#include <filesystem>

#include "gtest/gtest.h"
#include "line-count.hxx"

using namespace std;
using namespace std::literals;
using namespace ribomation;
using namespace testing;
namespace fs = std::filesystem;

struct GeneratedFile : Test {
     string const filename = "/tmp/test-data.txt"s;

protected:
    void SetUp() override {
        auto f = ofstream{filename};
        f << "this is the 1st line" << endl;
        f << "here comes the 2nd line" << endl;
        f << "let's throw in a 3rd line for good measures" << endl;
    }

    void TearDown() override {
        fs::remove(filename);
        ASSERT_FALSE(fs::exists(filename));
    }
};

TEST_F(GeneratedFile, simpleCount) {
    auto [lines, words, chars] = count(filename);

    EXPECT_EQ(lines, 3U);
    EXPECT_EQ(words, 19U);
    EXPECT_EQ(chars, 86U);
}

