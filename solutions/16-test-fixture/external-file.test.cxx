#include <filesystem>
#include <stdexcept>

#include "gtest/gtest.h"
#include "line-count.hxx"

using namespace std;
using namespace std::literals;
using namespace ribomation;
using namespace testing;
namespace fs = std::filesystem;

struct ExternalFile : Test {
    string const filename = "../line-count.hxx"s;

    ExternalFile() {
        auto p = fs::path{filename};
        if (!fs::exists(p)) {
            throw invalid_argument{"cannot open "s + filename};
        }
    }
};


TEST_F(ExternalFile, simpleCount) {
    auto [lines, words, chars] = count(filename);

    EXPECT_EQ(lines, 31U);
    EXPECT_EQ(words, 85U);
    EXPECT_EQ(chars, 739U);
}

