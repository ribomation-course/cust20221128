# Preparation Tasks
Here are some programming exercises to perform before the course starts.
Don't hesitate to [contact me](mailto:jens.riboe@ribomation.se) if you have en questions.


## Edit & Compile
First install the C++ compiler and the environment, according to the
installation instructions.

Use your favorit editor/IDE to write your code. Compile it on the command-line

    g++ -std=c++17 -Wall -Wextra -Werror -Wfatal-errors my-pgm.cxx -o my-pgm


# Simple Tasks
Here are a few _newbie_ tasks.

## Hello
Write a program that prints out a message to yourself and the result of `40 + 2`.

## Sum
Write a program having a function the uses 
[Gauss summation formula](https://en.wikipedia.org/wiki/1_%2B_2_%2B_3_%2B_4_%2B_%E2%8B%AF)

    SUM(1..N) = 1+2+3+...+N = N * (N+1) / 2

to compute the sum up to N, where N is provided on the command-line.

    int main(int argc, char** argv) {
        int N = argc==1 ? 10 : std::stoi(argv[1]);
        ...
    }

https://en.cppreference.com/w/cpp/string/basic_string/stol

## Fibonacci
Write a program having a double-recursive function that computes a 
[Fibonacci number](https://en.wikipedia.org/wiki/Fibonacci_number)

    FIB(0) = 0
    FIB(1) = 1
    FIB(N) = FIB(N-2) + FIB(N-1), N>1

Take N from the command-line.

## Count
Write a program that read line-by-line from stdin and counts the number of lines and characters.

    for (std::string line; getline(cin, line);) { ... } 

It should print out the result.

https://en.cppreference.com/w/cpp/string/basic_string/getline

# Simple Classes
Here are a few tasks that involve classes.

## Person
Write a Person class with name, email and age.

    std::string  name;
    std::string  email;
    unsigned     age;

Populate the member variables using a constructor. Write toString method that returns 
a string representation of a person and use it to print out a person object.

    std::string  toString() const { ... }

Create a person object and print it out.

## Person with a Dog
Write a Dog class, with a name and augment the Person class, so it might have a dog.
Create a dog object, attach it to a person object and print out.

## Overloading
Overload the left-shift operator to print out a person and his/her dog.

    ostream&  operator<<(ostream& os, const Person& p) { ... }

Overload the ++ operator for class Person, to increment age and return the updated age.

    int operator++() { ... }

# Templates

## Minimum
Write a template function that compare three values of the same type and return
the minimum value. Test with int, double and std::string.

## Stack
Write a simple stack template class, using an array.

    template<typename T, unsigned CAPACITY>
    class Stack {
      T    stk[CAPACITY];
      int  top = 0;
      public:
      bool  empty() const {...}
      bool  full() const {...}
      void  push(T x) {...}
      T     pop() {...}
    }

Test with int, double and std::string.

# STL

## Vector
Write a program that read line-by-line from stdin, pushing each line to a `std::vector`
and then print out the lines in reverse order with line number.

https://en.cppreference.com/w/cpp/container/vector

## Set
Write a program that read word-by-word from stdin, inserting each word (i.e. non-space chars)
into a `std::set` and then print out its content.

    for (std::string word; cin >> word; ) { ... }

https://en.cppreference.com/w/cpp/container/set

## Map
Write a program that read word-by-word from stdin, inserting each word into a `std::map`,
using `std::string` as key and `unsigned` as value.

https://en.cppreference.com/w/cpp/container/map

