#pragma once

#include <iostream>
#include <cstddef>

using namespace std;
using namespace std::literals;

class Blob {
    unsigned megaBytes = 0;
    byte*    payload   = nullptr;

public:
    Blob(unsigned megaBytes) : megaBytes{megaBytes}, payload{new byte[megaBytes * (1024U * 1024U)]} {
        cerr << "CREATE Blob{" << megaBytes << "MB} @ " << this
             << ", &payload=" << payload << endl;
    }

    ~Blob() {
        cerr << "~Blob() @ " << this;
        if (payload != nullptr) {
            delete[] payload;
            cerr << ", " << megaBytes << "MB disposed" << ", &payload=" << payload;
        }
        cerr << endl;
    }

    Blob(Blob&& that) : megaBytes{that.megaBytes}, payload{that.payload} {
        that.megaBytes = 0;
        that.payload   = nullptr;
        cerr << "CREATE Blob{&&" << megaBytes << "MB} @ " << this
             << ", &payload=" << payload << ", moved from Blob @ " << &that << endl;
    }

    Blob& operator =(Blob&& that) {
        cerr << "operator =(Blob&&) @ " << this;
        if (this != &that) {
            if (payload != nullptr) {
                delete[] payload;
                cerr << ", " << megaBytes << "MB disposed" << ", &payload=" << payload;
            }
            megaBytes = that.megaBytes;
            that.megaBytes = 0;
            payload = that.payload;
            that.payload = nullptr;
            cerr << ", " << megaBytes << "MB moved from " << &that << ", &payload=" << payload;
        }
        cerr << endl;
        return *this;
    }

    Blob() = delete;
    Blob(const Blob&) = delete;
    Blob& operator =(const Blob&) = delete;

    friend ostream& operator <<(ostream& os, const Blob& blob) {
        return os << "Blob{" << blob.megaBytes << "MB}";
    }
};

