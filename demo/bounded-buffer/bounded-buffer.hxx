#pragma once
#include <stdexcept>

namespace ribomation::data {

    template<typename ElemType, auto CAPACITY>
    class BoundedBuffer {
        ElemType storage[CAPACITY]{};
        unsigned putIdx = 0;
        unsigned getIdx = 0;
        unsigned count = 0;
    public:
        auto size() const { return count; }
        auto capacity() const { return CAPACITY; }
        bool empty() const { return size() == 0; }
        bool full() const { return size() == capacity(); }

        void put(ElemType x) {
            storage[putIdx] = x;
            putIdx = (putIdx + 1) % CAPACITY;
            if (full()) {
                getIdx = (getIdx + 1) % CAPACITY;
            } else {
                ++count;
            }
        }

        ElemType get() {
            if (empty()) {
                throw std::runtime_error{"buffer empty"};
            }
            auto x = storage[getIdx];
            getIdx = (getIdx + 1) % CAPACITY;
            --count;
            return x;
        }
    };

}


