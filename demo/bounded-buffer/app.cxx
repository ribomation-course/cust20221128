#include <iostream>
#include "bounded-buffer.hxx"

using std::cout;
using ribomation::data::BoundedBuffer;

void uc_ints() {
    cout << "-- ints --\n";
    constexpr auto N = 5U;
    auto buf = BoundedBuffer<unsigned, N>{};
    for (auto k = 1U; k <= N; ++k) buf.put(k);
    while (!buf.empty()) cout << buf.get() << " ";
    cout << "\n";
}

void uc_ints_overwrite() {
    cout << "-- ints overwrite --\n";
    constexpr auto N = 5U;
    auto buf = BoundedBuffer<unsigned, N>{};
    for (auto k = 1U; k <= N * 2; ++k) buf.put(k);
    while (!buf.empty()) cout << buf.get() << " ";
    cout << "\n";
}

void uc_ints_empty() {
    cout << "-- ints empty --\n";
    auto buf = BoundedBuffer<unsigned, 5U>{};
    try {
        auto x = buf.get();
        cout << "NOT expected: " << x << "\n";
    } catch (std::runtime_error &x) {
        cout << "expected: " << x.what() << "\n";
    }
}

void uc_ints_stream() {
    cout << "-- ints stream --\n";
    auto buf = BoundedBuffer<unsigned, 5U>{};
    auto const N = 100U;
    for (auto k = 1U; k <= N; ++k) {
        buf.put(k);
        cout << buf.get() << " ";
    }
    cout << "\n";
}

int main() {
    uc_ints();
    uc_ints_overwrite();
    uc_ints_empty();
    uc_ints_stream();
}
