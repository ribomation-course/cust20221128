# Installation Instructions

To perform the programming exercises, you need:
* Modern C++ compiler, supporting C++ 17, _to compile your programs_
* Decent C++ IDE, _to write your code_
* GIT client, _to easily get the solutions from this repo_


## Installation of Ubuntu Linux
We will do the exercises on Linux. Therefore you need access to a Linux or Unix system, preferably Ubuntu.
Read our common guide of how to install Linux on WSL and a C++ compiler.

* [Linux and C++ Installation](https://gitlab.com/ribomation-course/common-instructions/-/blob/master/linux-and-cxx.md)


## Installation within *NIX
* The GNU C/C++ compiler, version 11 or later; `sudo apt install g++-11` or `sudo apt install g++-12`
* The CMake builder tools; `sudo apt install make cmake`
* Helpers; `sudo apt install gdb valgrind tree`
* Git client; `sudo apt install git`

_N.B._ when you run a `sudo` command it prompts you for the password, you use
to log on to Ubuntu. If you're running another OS, amend the installation commands accordingly.
